import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1
import QtWebEngine 1.0
import QtGraphicalEffects 1.0

Item {
    TextField {
        id: textBox
        z: 1
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.leftMargin: 40
        anchors.rightMargin: 40
        height: 20

        onEditingFinished: {
            webView.url = text
        }
    }

    Button {
        id: backBtn
        z: 1
        width: 30
        height: 30
        anchors.top: parent.top
        anchors.left: parent.left
        iconSource: "qrc:/images/arrow_left.png"

        onClicked: {
            webView.goBack();
        }
    }

    WebEngineView {
        id: webView
        anchors.bottom: bottomToolbar.top
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        url: "https://www.google.fi"
    }

    ToolBar {
        id: bottomToolbar
        anchors.bottom: parent.bottom
        anchors.bottomMargin: scale * bottomMargin
        width: parent.width
        scale: (Qt.platform.os === "android" || Qt.platform.os === "ios") ? 2.0 : 1.0

        RowLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: columnSpacing
            ToolButton {
                iconSource: "qrc:/images/arrow_left.png"
                onClicked: {
                    stackView.pop();
                }
            }
        }
        style: ToolBarStyle {
            background: Item {
            }
        }
    }
}

