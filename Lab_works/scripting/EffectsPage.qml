import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0
import QtQuick.Particles 2.0

Item {
    property string fileId
    property bool imageEffectOn: false

    onFileIdChanged: {
        image.source = useEnginio ? "" : fileId
        if (useEnginio) {
            // Download the full image, not the thumbnail
            var data = { "id": fileId }
            console.log("Data", data);
            var reply = enginioClient.downloadUrl(data)
            reply.finished.connect(function() {
                image.source = reply.data.expiringUrl
            })
        }
    }

    Label {
        id: label
        text: qsTr("Loading ...")
        font.pixelSize: 28
        color: "white"
        anchors.centerIn: parent
        visible: image.status != Image.Ready
    }

    Image {
        id: image
        anchors.top: parent.top
        anchors.bottom: bottomToolbar.top
        anchors.margins: 10
        anchors.horizontalCenter: parent.horizontalCenter
        smooth: true
        cache: false
        fillMode: Image.PreserveAspectFit
        Behavior on opacity { NumberAnimation { duration: 100 } }
        opacity: image.status === Image.Ready ? 1 : 0
    }

    Glow {
        visible: imageEffectOn
        anchors.fill: image
        source: image
        radius: 8
        samples: 17
        color: "white"
    }

    ParticleSystem {
        anchors.top: parent.top
        anchors.bottom: bottomToolbar.top
        anchors.left: parent.left
        anchors.right: parent.right

        ImageParticle {
            source: "qrc:/images/blueStar.png"
            colorVariation: 0.5
        }

        Emitter {
            id: particleEmitter
            anchors.centerIn: parent
            emitRate: 0
            lifeSpan: 500
            lifeSpanVariation: 1000
            velocity: AngleDirection {
                angle: 0
                angleVariation: 180
                magnitude: 60
                magnitudeVariation: 50
            }
        }

        Gravity {
            magnitude: 200
        }
    }

    ToolBar {
        id: bottomToolbar
        anchors.bottom: parent.bottom
        anchors.bottomMargin: scale * bottomMargin
        width: parent.width
        scale: (Qt.platform.os === "android" || Qt.platform.os === "ios") ? 2.0 : 1.0

        RowLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: columnSpacing
            ToolButton {
                iconSource: "qrc:/images/arrow_left.png"
                onClicked: {
                    stackView.pop();
                }
            }
            ToolButton {
                iconSource: "qrc:/images/pen.png"
                onClicked: {
                    imageEffectOn = !imageEffectOn
                    if (!imageEffectOn) {
                        particleEmitter.burst(1000);
                        image.opacity = 0.0
                    }
                }
            }
        }
        style: ToolBarStyle {
            background: Item {
            }
        }
    }
}

