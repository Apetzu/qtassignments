import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.1

Item {
    ToolBar {
        width: parent.width
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 15
        RowLayout {
            anchors.horizontalCenter: parent.horizontalCenter
            ToolButton {
                id: imageViewBtn
                onClicked: stackView.pop()
                implicitWidth: 120

                Text {
                    text: "Back"
                    color: "white"
                    font.pointSize: 20
                    anchors.centerIn: parent
                }
            }
        }

        style: ToolBarStyle { background: Item {} }
    }
}
