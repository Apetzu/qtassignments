import QtQuick 2.5
import QtQuick.Controls 1.4

ApplicationWindow {
    id: appWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Image App")

    Rectangle {
        anchors.fill: parent
        gradient: Gradient {
            GradientStop {
                position: 0.00
                color: "#000000"
            }
            GradientStop {
                position: 1.00
                color: "#b9b3b3"
            }
        }
    }

    NotificationMessage {
        anchors.fill: parent
        Component.onCompleted: show(qsTr("Notification"));
    }

    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: "qrc:/MainPage.qml"
    }
}
