import QtQuick 2.5
import QtQuick.Controls 1.4

ApplicationWindow {
    id: appWindow
    visible: true
    width: 640
    height: 480
    title: qsTr("Image App")

    Notification {
        anchors.centerIn: parent
        Component.onCompleted: {
            show(qsTr("Some text"));
        }
    }
}
