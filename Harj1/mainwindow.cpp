#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    bool ok = connect(ui->calcButton, SIGNAL(clicked()), this, SLOT(onCalculate()));
    Q_ASSERT(ok);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onCalculate()
{
    bool widthOk = false;
    bool heightOk = false;

    int width = ui->widthInput->text().toInt(&widthOk, 10);
    int height = ui->heightInput->text().toInt(&heightOk, 10);

    if (widthOk && heightOk)
    {
        QString area = QString::number(width * height);
        ui->areaInput->setText(area);
    }
    else
    {
        ui->areaInput->setText("");
    }
}
