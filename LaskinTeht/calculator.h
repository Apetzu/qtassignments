#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QWidget>

QT_BEGIN_NAMESPACE
namespace Ui
{
    class CalculatorUI;
}
QT_END_NAMESPACE

class Calculator : public QWidget
{
    Q_OBJECT

public:
    Calculator(QWidget *parent = nullptr);
    ~Calculator();

private:
    enum CalcOpr
    {
        OPR_NONE = -1,
        OPR_PLUS,
        OPR_MINUS,
        OPR_MUL,
        OPR_DIV
    };

    void calculate(double secondValue);
    double convertStrToNum(const QString &str);
    void showResult();

    Ui::CalculatorUI *ui;
    QString numBuffer;
    bool firstNumEntered;
    bool numEntered;
    double result;
    CalcOpr lastOpr;

private slots:
    void numBtnPressed(char character);
    void oprPressed(CalcOpr opr);
    void equalsPressed();
    void clear();
};

#endif // CALCULATOR_H
