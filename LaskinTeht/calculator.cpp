#include "calculator.h"
#include "ui_calculator.h"

#include <QSignalMapper>
#include <QMessageBox>

Calculator::Calculator(QWidget *parent) : QWidget(parent), ui(new Ui::CalculatorUI), firstNumEntered(false), numEntered(false), result(0.0), lastOpr(OPR_NONE)
{
    ui->setupUi(this);

    connect(ui->btn0, &QPushButton::clicked, [=]() { emit numBtnPressed('0'); });
    connect(ui->btn1, &QPushButton::clicked, [=]() { emit numBtnPressed('1'); });
    connect(ui->btn2, &QPushButton::clicked, [=]() { emit numBtnPressed('2'); });
    connect(ui->btn3, &QPushButton::clicked, [=]() { emit numBtnPressed('3'); });
    connect(ui->btn4, &QPushButton::clicked, [=]() { emit numBtnPressed('4'); });
    connect(ui->btn5, &QPushButton::clicked, [=]() { emit numBtnPressed('5'); });
    connect(ui->btn6, &QPushButton::clicked, [=]() { emit numBtnPressed('6'); });
    connect(ui->btn7, &QPushButton::clicked, [=]() { emit numBtnPressed('7'); });
    connect(ui->btn8, &QPushButton::clicked, [=]() { emit numBtnPressed('8'); });
    connect(ui->btn9, &QPushButton::clicked, [=]() { emit numBtnPressed('9'); });
    connect(ui->btnDot, &QPushButton::clicked, [=]() { emit numBtnPressed('.'); });
    connect(ui->btnBack, &QPushButton::clicked, [=]() { emit numBtnPressed('-'); });
    connect(ui->btnPlus, &QPushButton::clicked, [=]() { emit oprPressed(OPR_PLUS); });
    connect(ui->btnMinus, &QPushButton::clicked, [=]() { emit oprPressed(OPR_MINUS); });
    connect(ui->btnMul, &QPushButton::clicked, [=]() { emit oprPressed(OPR_MUL); });
    connect(ui->btnDiv, &QPushButton::clicked, [=]() { emit oprPressed(OPR_DIV); });
    connect(ui->btnEqual, &QPushButton::clicked, [=]() { emit equalsPressed(); });
    connect(ui->btnClear, &QPushButton::clicked, [=]() { emit clear(); });
}

Calculator::~Calculator()
{
    delete ui;
}

void Calculator::numBtnPressed(char character)
{
    if ((character >= '0' && character <= '9') || character == '.')
    {
        numBuffer += character;
        numEntered = true;
        ui->resultBox->setPlainText(numBuffer);
    }
    else if (character == '-')
    {
        if (numBuffer.size() > 0 && numEntered)
        {
            numBuffer = numBuffer.left(numBuffer.size() - 1);
            ui->resultBox->setPlainText(numBuffer);
        }
    }
}

void Calculator::oprPressed(CalcOpr opr)
{
    if (numEntered)
    {
        double convertedNum = convertStrToNum(numBuffer);

        numBuffer = "";

        if (!firstNumEntered)
        {
            firstNumEntered = true;
            result = convertedNum;
        }
        else
        {   
            calculate(convertedNum);
        }

        showResult();
    }

    lastOpr = opr;

    numEntered = false;
}

void Calculator::equalsPressed()
{
    if (numEntered)
    {
        calculate(convertStrToNum(numBuffer));
        numBuffer = "";
    }

    firstNumEntered = false;
    lastOpr = OPR_NONE;
    showResult();

    numEntered = false;
}

void Calculator::clear()
{
    firstNumEntered = false;
    numEntered = false;
    numBuffer = "";
    result = 0.0;
    lastOpr = OPR_NONE;
    ui->resultBox->setPlainText("");
}

void Calculator::calculate(double secondValue)
{
    switch (lastOpr)
    {
    case OPR_PLUS:
        result += secondValue;
        break;
    case OPR_MINUS:
        result -= secondValue;
        break;
    case OPR_MUL:
        result *= secondValue;
        break;
    case OPR_DIV:
        result /= secondValue;
        break;
    default:
        break;
    }
}

double Calculator::convertStrToNum(const QString &str)
{
    bool convertOk;
    double convertedNum = str.toDouble(&convertOk);
    if (!convertOk)
    {
        QMessageBox errorMessage;
        errorMessage.critical(0, "Error", "String to double conversion failed.");
        return 0.0;
    }

    return convertedNum;
}

void Calculator::showResult()
{
    if (qIsInf(result) || qIsNaN(result))
    {
        clear();
        ui->resultBox->setPlainText("Undefined");
    }
    else
    {
        ui->resultBox->setPlainText(QString::number(result));
    }
}
